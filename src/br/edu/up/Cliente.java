package br.edu.up;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.SocketException;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;

public class Cliente {
	
	public static void main(String[] args) throws SocketException, IOException {
		
		//1. Estabelecer a conex�o com o servidor FTP.
		FTPClient client = new FTPClient();
		client.connect("localhost", 9999);
		client.login("root", "123");
		client.setFileType(FTP.BINARY_FILE_TYPE);
		client.enterLocalPassiveMode();
				
		//2. Pegar a listagem de arquivos.
		FTPFile[] ftpFiles = client.listFiles("/download");
		String arquivo = "arquivobaixar.txt";
		
		boolean arquivoExiste = false;
		for (FTPFile ftpFile : ftpFiles) {
			String nome = ftpFile.getName();
			if (arquivo.equals(nome)){
				arquivoExiste = true;
				break;
			}
		}
		
		//3. Baixar os arquivos.
		if (arquivoExiste){
		   File arquivoBaixado = new File("d:/CTI/arquivobaixado.txt");
		   FileOutputStream fos = new FileOutputStream(arquivoBaixado);
		   client.retrieveFile("/download/arquivobaixar.txt", fos);
	
		 //4. Excluir os arquivos baixados.
		   client.deleteFile("/download/arquivobaixar.txt");
		}
		
		
		//5. Desconectar do servidor.
		if (client.isConnected()){
			client.logout();
			client.disconnect();
		}
	}

}
